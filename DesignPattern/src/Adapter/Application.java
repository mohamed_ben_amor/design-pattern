package Adapter;

public class Application {
	
	public static void main(String [] args)
	{
		IStandard standard1 = new StandardImpl1();
		standard1.operation(7, 9);
		
		IStandard standard2 = new AdapterHeritage();
		standard2.operation(7, 9);
		
		IStandard standard3 = new AdapterCompos();
		standard3.operation(7, 9);
	}

}
