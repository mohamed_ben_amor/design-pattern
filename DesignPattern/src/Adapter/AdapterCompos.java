package Adapter;

import Adapter.ext.ImplementationAdaptee;

public class AdapterCompos implements IStandard  {

	ImplementationAdaptee adaptee = new ImplementationAdaptee(); 
	@Override
	public void operation(int nb1, int nb2) {
		int nb = adaptee.operation2(nb1, nb2);
		adaptee.operation3(nb);
		
	}

}
