package Adapter;

import Adapter.ext.ImplementationAdaptee;

public class AdapterHeritage extends ImplementationAdaptee implements IStandard {

	@Override
	public void operation(int nb1, int nb2) {
	   int res = operation2(nb1, nb2);
	   operation3(res);
		
	}

}
