package Composite;

import java.util.ArrayList;
import java.util.List;

public class Composite extends Composant {
	
	public Composite(String nom) {
		super(nom);
		// TODO Auto-generated constructor stub
	}

	private List<Composant> composants=new ArrayList<Composant>();

	@Override
	public void operation() {
		String tab = "";
		for(int i=0;i<niveau;i++) tab+="----";
		
		System.out.println(tab+" Composite: "+nom);
		for(Composant c:composants)
		{
			c.operation();
		}
	}
	
	public void add(Composant c)
	{
		c.niveau=this.niveau+1;
		composants.add(c);
	}
	
	public void remove(Composant c)
	{
		composants.remove(c);
	}
	
	public List<Composant> getChilds()
	{
		return composants;
	}

}
