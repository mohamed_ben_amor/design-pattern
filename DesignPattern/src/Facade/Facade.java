package Facade;

public class Facade {
	
	private ClasseA a = new ClasseA();
	private ClasseB b = new ClasseB();
	
	public void operation2()
	{
		System.out.println("Opeartion 2 of Facade");
		a.operation2();
	}
	
	public void opearation41()
	{
		System.out.println("Operation 41 of Facade");
		a.operation1();
		b.operation4();
	}
	

}
