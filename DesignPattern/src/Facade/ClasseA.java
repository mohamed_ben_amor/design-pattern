package Facade;

public class ClasseA {
	
	public void operation1()
	{
		System.out.println("operation 1 of class A");
	}
	
	public void operation2()
	{
		System.out.println("operation 2 of class A");
	}

}
