package FlyWeight;

enum WeaponType{
	Gun,
	Sword	
}


public abstract class Soldier
{        
          public WeaponType weapon;
          public abstract void RenderSoldier(String StrPriName, String Color);
}
