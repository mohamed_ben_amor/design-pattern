package FlyWeight;

import java.util.HashMap;
import java.util.Map;

public class SoldierFactory {
	 Map<String, Soldier> soldierCollection;
     public SoldierFactory()
     {
               soldierCollection = new HashMap<String, Soldier>();
     }
     private Soldier getSoldier(String soliderType)
     {                 
               if(!soldierCollection.containsKey(soliderType))
               {
                         System.out.println("Object created");
                         switch(soliderType)
                         {
                                   case "GunSolider":
                                   soldierCollection.put(soliderType, new GunSoldier());
                                   break;
                                   case "SwordSolider":
                                   soldierCollection.put(soliderType, new SwordSoldier());
                                   break;
                         }
               }
               else
               {
                         System.out.println("Object already created");
               }
               return soldierCollection.get(soliderType);
     }
     
     public void CreateSoldier(String SoliderType, String characterName, String txtColor)
     {
               Soldier soldier = getSoldier(SoliderType);
               soldier.RenderSoldier(characterName,txtColor);
     }

}
