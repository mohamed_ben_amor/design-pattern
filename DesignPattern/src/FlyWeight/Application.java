package FlyWeight;

public class Application {

	public static void main(String[] args)
	{
		SoldierFactory solidierfactory = new SoldierFactory();
		
		solidierfactory.CreateSoldier("GunSolider", "Chiraz", "red");
		solidierfactory.CreateSoldier("SwordSolider", "Mohamed", "blue");
		
		solidierfactory.CreateSoldier("GunSolider", "monir", "black");
		solidierfactory.CreateSoldier("SwordSolider", "Ilyes", "green");
	}
	
}
