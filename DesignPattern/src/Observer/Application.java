package Observer;

public class Application {
	
	public static void main(String[] args)
	{
		ObservableConcret sujet = new ObservableConcret();
		ObserverImpl1 obs1= new ObserverImpl1();
		sujet.registerObserver(obs1);
		
		ObserverImpl2 obs2= new ObserverImpl2();
		sujet.registerObserver(obs2);
		
		sujet.setEtat(9);
		
		
	}

}
