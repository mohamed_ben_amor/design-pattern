package Observer;

import java.util.ArrayList;
import java.util.List;

public interface Observable {
	
	public void registerObserver(Observer o);
	public void deleteObserver(Observer o);
	public void notifyObservers();

}
