package Observer;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Administrateur
 *
 */
public class ObservableConcret implements Observable {
	
	private List<Observer> observers = new ArrayList<>();
	private int etat;

	@Override
	public void registerObserver(Observer o) {
		observers.add(o);
	}

	@Override
	public void deleteObserver(Observer o) {
		observers.remove(o);
		
	}

	@Override
	public void notifyObservers() {
		for(Observer o: observers)
		{
			o.update(this);
			// o.update(etat);
		}
		
	}

	public int getEtat() {
		return etat;
	}

	public void setEtat(int etat) {
		this.etat = etat;
		notifyObservers();
	}
	
	

}
