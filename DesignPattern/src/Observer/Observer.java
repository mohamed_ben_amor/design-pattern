package Observer;

public interface Observer {
	
	// we have here two options pull (tirer) and push
	// public void update(Observable o) is the pull method
	// public void update(int v) is the pull method
	public void update(Observable o);
	
	

}
