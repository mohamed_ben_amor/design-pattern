package Decorator;

public class Sumatra extends Boisson {
	
	public Sumatra() {
		description="Sumatra";
	}

	@Override
	public double cout() {
		return 5.9;
	}

}
