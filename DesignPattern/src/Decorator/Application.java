package Decorator;

import Decorator.decorators.Chantilly;
import Decorator.decorators.Lait;

public class Application {
	
	public static void main(String[] args)
	{
		
		Boisson b1=new Expresso();
		
		System.out.println(b1.getDescription()+" $ "+b1.cout());
		
		Boisson b2 = new Deca();
		System.out.println(b2.getDescription()+" $ "+b2.cout());
		b2 = new Lait(b2);
		System.out.println(b2.getDescription()+" $ "+b2.cout());
		b2 = new Chantilly(b2);
		
		System.out.println(b2.getDescription()+" $ "+b2.cout());
		
		
	}

}
