package Decorator.decorators;

import Decorator.Boisson;

public abstract class DecoratorIngredient extends Boisson {

	public Boisson boisson;

	public DecoratorIngredient(Boisson boisson) {
		super();
		this.boisson = boisson;
	}

	public abstract String getDescription();

}
