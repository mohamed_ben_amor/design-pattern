package Decorator.decorators;

import Decorator.Boisson;

public class Chantilly extends DecoratorIngredient {

	public Chantilly(Boisson boisson) {
		super(boisson);

	}

	@Override
	public String getDescription() {

		return boisson.getDescription() + ", chantilly";
	}

	@Override
	public double cout() {

		return 0.5 + boisson.cout();
	}

}
