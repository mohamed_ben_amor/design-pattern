package Decorator.decorators;

import Decorator.Boisson;

public class Lait extends DecoratorIngredient {

	public Lait(Boisson boisson) {
		super(boisson);
		

	}

	@Override
	public String getDescription() {
	
		return boisson.getDescription() + ", Lait";
		
	}

	@Override
	public double cout() {
		return 2.5 + boisson.cout();
	}

}
